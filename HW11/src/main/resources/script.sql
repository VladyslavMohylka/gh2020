/* Task 1 *//*
SELECT id, sortname, name, phonecode, quantity_states
  FROM countries
       JOIN (
              SELECT country_id AS id,
                     COUNT(country_id) AS quantity_states
                FROM states
            GROUP BY country_id
            ORDER BY quantity_states DESC
               LIMIT 1
            ) AS max_quantity_states USING(id);
*/
/* Task 2 *//*
SELECT id, sortname, name, phonecode
  FROM countries
 WHERE id = (
              SELECT country_id
                FROM states
                     JOIN (
                            SELECT state_id AS id,
                                   COUNT(state_id) AS quantity_cities
                              FROM cities
                          GROUP BY state_id
                          ) AS state_quantity_cities USING(id)
            GROUP BY country_id
            ORDER BY SUM(quantity_cities) DESC
               LIMIT 1;
            )
*/
/* Task3 *//*
  SELECT id, sortname, name, phonecode, quantity_states
    FROM countries
         JOIN (
                SELECT country_id as id,
                       COUNT(country_id) AS quantity_states
                  FROM states
              GROUP BY country_id
              ) AS country_quantity_states USING(id)
ORDER BY quantity_states DESC, name, id;
*/
/* Task4 *//*
  SELECT id, sortname, name, phonecode, quantity_cities_country
    FROM countries
         JOIN (
                SELECT country_id AS id,
                       SUM(quantity_cities_state) AS quantity_cities_country
                  FROM states
                       JOIN (
                               SELECT state_id AS id,
                                      COUNT(state_id) AS quantity_cities_state
                                 FROM cities
                             GROUP BY state_id
                             ) AS state_quantity_cities USING(id)
              GROUP BY country_id
              ) AS country_quantity_cities USING(id)
GROUP BY id, quantity_cities_country
ORDER BY quantity_cities_country DESC;
*/
/* Task 5 *//*
  SELECT id, sortname, name, phonecode, quantity_states, quantity_cities_country
    FROM countries
         JOIN (
                SELECT country_id AS id,
                       COUNT(country_id) AS quantity_states
                  FROM states
              GROUP BY country_id
              ) AS country_quantity_states USING(id)
         JOIN (
                SELECT country_id AS id,
                       SUM(quantity_cities_state) AS quantity_cities_country
                  FROM states
                       JOIN (
                              SELECT state_id AS id,
                                     COUNT(state_id) AS quantity_cities_state
                                FROM cities
                            GROUP BY state_id
                            ) AS state_quantity_cities USING(id)
              GROUP BY country_id
              ) AS country_quantity_cities USING(id)
GROUP BY id, sortname, name, phonecode, quantity_states, quantity_cities_country;
*/
/* Task6 *//*
SELECT id, sortname, name, phonecode
  FROM countries
       JOIN (
              SELECT state_id as id,
                     COUNT(state_id) AS quantity_cities
                FROM cities
            GROUP BY state_id
            ORDER BY quantity_cities DESC
            ) AS state_quantity_cities USING(id)
 LIMIT 10;*/
/* Task 7 *//*
(
  SELECT id, sortname, name, phonecode, quantity_states
    FROM countries
         JOIN (
                SELECT country_id AS id,
                       COUNT(country_id) AS quantity_states
                  FROM states
              GROUP BY country_id
              ORDER BY quantity_states DESC
                 LIMIT 10
              ) AS country_max_quantity_states USING(id)
ORDER BY name
)
UNION ALL
(
  SELECT id, sortname, name, phonecode, quantity_states
    FROM countries
         JOIN (
                SELECT country_id AS id,
                       COUNT(country_id) AS quantity_states
                  FROM states
              GROUP BY country_id
              ORDER BY quantity_states
                 LIMIT 10
              ) AS country_min_quantity_states USING(id)
ORDER BY name
);
*/
/* Task 8 *//*
WITH country_id_over_avg AS (
    WITH country_quantity_states AS (
        SELECT states.country_id AS id,
               COUNT(states.country_id) AS quantity_states
          FROM states
      GROUP BY country_id
    )
      SELECT id
        FROM country_quantity_states
    GROUP BY id
      HAVING id > AVG(quantity_states)
)
  SELECT countries.id, sortname, name, phonecode
    FROM countries, country_id_over_avg
   WHERE countries.id = country_id_over_avg.id
GROUP BY countries.id;
*/
/* Task9 *//*
WITH country_quantity_states AS (
    SELECT states.country_id AS id,
           COUNT(states.country_id) AS quantity_states
      FROM states
  GROUP BY country_id
    )
    SELECT DISTINCT ON (quantity_states) countries.id, sortname, name, phonecode, quantity_states
      FROM countries, country_quantity_states
     WHERE countries.id = country_quantity_states.id
  GROUP BY countries.id, quantity_states
  ORDER BY quantity_states;*/
/* Task 10 *//*
WITH state_quantity_repeat AS (
    SELECT name,
           COUNT(*) AS quantity_repeat
      FROM states
  GROUP BY name
    HAVING COUNT(*) > 1
)
  SELECT id, name, country_id
    FROM states
   WHERE name IN (SELECT name FROM state_quantity_repeat)
ORDER BY name, id;
*/
/* Task 11 *//*
SELECT id, name, country_id
  FROM states
 WHERE states.id NOT IN (
                        SELECT state_id
                          FROM cities
                         WHERE (SELECT COUNT(state_id) FROM cities) > 0
                        );
*/


