package com.geekhub.hw9.task1.example;

import java.util.Objects;

public class Car {
    private final String color;
    private final int maxSpeed;
    private final String type;
    private final String model;

    public Car(String color, int maxSpeed, String type, String model) {
        this.color = color;
        this.maxSpeed = maxSpeed;
        this.type = type;
        this.model = model;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return maxSpeed == car.maxSpeed &&
                Objects.equals(color, car.color) &&
                Objects.equals(type, car.type) &&
                Objects.equals(model, car.model);
    }

    @Override
    public int hashCode() {
        return Objects.hash(color, maxSpeed, type, model);
    }

    @Override
    public String toString() {
        return "Car{" +
                "color='" + color + '\'' +
                ", maxSpeed=" + maxSpeed +
                ", type='" + type + '\'' +
                ", model='" + model + '\'' +
                '}';
    }
}
