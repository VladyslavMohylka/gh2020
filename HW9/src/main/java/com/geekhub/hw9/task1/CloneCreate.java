package com.geekhub.hw9.task1;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class CloneCreate {
    public <E> Object getClone(E object) {
        if (object == null) {
            return null;
        }
        try {
            Object objectClone = createObject(object.getClass());
            cloneFields(object, objectClone);

            return objectClone;
        } catch (Exception e) {
            System.err.println(e.getClass().getSimpleName() + ", error in method: getClone()");

            return null;
        }
    }

    private <E> E createObject(Class<E> classOrigin) throws IllegalAccessException, InvocationTargetException, InstantiationException {
        try {
            return classOrigin.getDeclaredConstructor().newInstance();
        } catch (Exception e) {
            Constructor<?>[] constructorsOrigin = classOrigin.getConstructors();
            List<Object> args = new ArrayList<>();

            for (Class<?> parameterType : constructorsOrigin[0].getParameterTypes()) {
                args.add(setParameterValue(parameterType));
            }

            return classOrigin.cast(constructorsOrigin[0].newInstance(args.toArray()));
        }
    }

    private Object setParameterValue(Class<?> type) {
        if (type.isPrimitive()) {
            return 0;
        } else {
            return null;
        }
    }

    private <E> void cloneFields(E object, E objectClone) throws IllegalAccessException {
        for (Field field : object.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            field.set(objectClone, field.get(object));
        }
    }
}
