package com.geekhub.hw9.task1.example;

import java.util.Objects;

public class Cat {
   private String color;
   private int age;
   private int legCount;
   private int fullLength;

    public Cat() {
    }

    public Cat(String color, int age, int legCount, int fullLength) {
        this.color = color;
        this.age = age;
        this.legCount = legCount;
        this.fullLength = fullLength;
    }

    public void setFullLength(int fullLength) {
        this.fullLength = fullLength;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setLegCount(int legCount) {
        this.legCount = legCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cat cat = (Cat) o;
        return age == cat.age &&
                legCount == cat.legCount &&
                fullLength == cat.fullLength &&
                Objects.equals(color, cat.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(color, age, legCount, fullLength);
    }

    @Override
    public String toString() {
        return "Cat{" +
                "color = '" + color + '\'' +
                ", age = " + age +
                ", legCount = " + legCount +
                ", fullLength = " + fullLength +
                '}';
    }
}
