package com.geekhub.hw9.task1.example;

import java.util.Objects;

public class Human {
    private final int height;
    private final String gender;
    private final int age;
    private final int weight;

    public Human(int height, String gender, int age, int weight) {
        this.height = height;
        this.gender = gender;
        this.age = age;
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return height == human.height &&
                age == human.age &&
                weight == human.weight &&
                Objects.equals(gender, human.gender);
    }

    @Override
    public int hashCode() {
        return Objects.hash(height, gender, age, weight);
    }

    @Override
    public String toString() {
        return "Human{" +
                "height=" + height +
                ", gender='" + gender + '\'' +
                ", age=" + age +
                ", weight=" + weight +
                '}';
    }
}
