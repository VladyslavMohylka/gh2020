package com.geekhub.hw9.task1;

import com.geekhub.hw9.task1.exception.DifferentClassesException;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BeanComparator {
    public <E> List<String[]> compare(E objectOne, E objectTwo) {
        try {
            if (objectOne == null || objectTwo == null) {
                throw new NullPointerException();
            }
            if (!objectOne.getClass().equals(objectTwo.getClass())) {
                throw new DifferentClassesException(objectOne.getClass().getSimpleName(), objectTwo.getClass().getSimpleName());
            }

            List<String[]> result = new ArrayList<>();
            Field[] fieldsOne = objectOne.getClass().getDeclaredFields();
            Field[] fieldsTwo = objectOne.getClass().getDeclaredFields();

            for (int i = 0; i < fieldsOne.length; i++) {
                String[] array = new String[4];

                fieldsOne[i].setAccessible(true);
                fieldsTwo[i].setAccessible(true);

                array[0] = fieldsOne[i].getName();
                array[1] = fieldsOne[i].get(objectOne).toString();
                array[2] = fieldsOne[i].get(objectTwo).toString();
                array[3] = String.valueOf(array[1].equals(array[2]));
                result.add(array);
            }

            return result;

        } catch (NullPointerException e) {
            System.err.println("Error! Any of the arguments is null in the method: compare().");
        } catch (DifferentClassesException e) {
        System.err.println(e.getMessage());
        } catch (IllegalAccessException e) {
            System.err.println(e.getClass().getSimpleName() + ", error in the method: compare().");
        }

        return Collections.emptyList();
    }
}
