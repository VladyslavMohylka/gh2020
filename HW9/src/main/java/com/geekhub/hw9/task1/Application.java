package com.geekhub.hw9.task1;

import com.geekhub.hw9.task1.example.Car;
import com.geekhub.hw9.task1.example.Cat;
import com.geekhub.hw9.task1.example.Human;

import java.util.Map;

public class Application {
    public static void main(String[] args) {
        Cat cat1 = new Cat("Black", 3, 4, 35);
        Cat cat2 = new Cat("White", 3, 4, 32);
        Car car = new Car("black", 190, "Sedan", "RX-7");
        Human human = new Human(180, "male", 22, 75);

        BeanRepresent beanRepresent = new BeanRepresent();
        Map<String, Object> beanRepresentFields = beanRepresent.getFields(car);
        //System.out.println(beanRepresentFields);

        CloneCreate cloneCreate = new CloneCreate();
        Human clonedObject = (Human) cloneCreate.getClone(human);
        //System.out.println(clonedObject.toString());
        //System.out.println(human.equals(clonedObject));

        BeanComparator beanComparator = new BeanComparator();
        /*for (String[] a : beanComparator.compare(cat1, cat2)) {
            System.out.println(Arrays.toString(a));
        }*/
    }
}
