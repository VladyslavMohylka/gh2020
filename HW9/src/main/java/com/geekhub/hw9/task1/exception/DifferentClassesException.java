package com.geekhub.hw9.task1.exception;

public class DifferentClassesException extends Exception {
    private final String message;

    public DifferentClassesException(String arg1, String arg2) {
        this.message = String.format("Error! Compare class %s with class %s", arg1, arg2);
    }

    @Override
    public String getMessage() {
        return message;
    }
}
