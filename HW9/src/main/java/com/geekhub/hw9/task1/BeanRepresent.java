package com.geekhub.hw9.task1;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class BeanRepresent {
    public <E> Map<String, Object> getFields(E object) {
        if (object == null) {
            return Collections.emptyMap();
        }
        try {
            Map<String, Object> result = new HashMap<>();
            Class<?> clazz = object.getClass();

            for (Field field : clazz.getDeclaredFields()) {
                field.setAccessible(true);
                result.put(field.getName(), field.get(object));
            }

            return result;
        } catch (IllegalAccessException e) {
            System.err.println(e.getClass().getSimpleName() + ", error in the method: getFields().");

            return Collections.emptyMap();
        }
    }
}
