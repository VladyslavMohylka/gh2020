package com.geekhub.hw9.task2.json.adapter;

import com.geekhub.hw9.task2.json.JsonSerializer;
import com.geekhub.hw9.task2.json.SerializationException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Converts all objects that extends java.util.Map to JSONObject.
 */
public class MapAdapter implements JsonDataAdapter<Map<?, ?>> {

    @Override
    public Object toJson(Map<?, ?> map) {
        if (map == null || map.isEmpty()) {
            return new JSONObject();
        }
        JSONObject jsonObject = new JSONObject();

        for (Map.Entry<?, ?> e : map.entrySet()) {
            try {
                jsonObject.put(String.valueOf(e.getKey()), JsonSerializer.serialize(e.getValue()));
            } catch (SerializationException exp) {
                System.err.println("SerializationException while serialize(): " + e.getKey() + " field by map");
                System.err.println(exp.getStackTrace()[0]);
            }
        }

        return jsonObject;
    }
}

