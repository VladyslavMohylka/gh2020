package com.geekhub.hw9.task2.json;

public class SerializationException extends Exception {

    public SerializationException(String message, Throwable cause) {
        super(message, cause);
    }
}

