package com.geekhub.hw9.task2.json.adapter;

import com.geekhub.hw9.task2.json.JsonSerializer;
import com.geekhub.hw9.task2.json.SerializationException;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.Collection;

/**
 * Converts all objects that extends java.util.Collections to JSONArray.
 */
public class CollectionAdapter implements JsonDataAdapter<Collection<?>> {

    @Override
    public Object toJson(Collection<?> c) throws JSONException {
        if (c == null || c.isEmpty()) {
            return new JSONArray();
        }
        JSONArray array = new JSONArray();

        for (Object o : c) {
            try {
                array.put(JsonSerializer.serialize(o));
            } catch (SerializationException exp) {
                System.err.println("SerializationException while serialize(): " + o + " element by collection");
                System.err.println(exp.getStackTrace()[0]);
            }
        }

        return array;
    }
}

