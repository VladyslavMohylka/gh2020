package com.geekhub.hw9.task2.json.adapter;

import com.geekhub.hw9.task2.json.SerializationException;

/**
 * JsonDataAdapter contains instructions how to serialize Java object to Json representation.
 *
 * @param <T> determines type adapter works with.
 */
public interface JsonDataAdapter<T> {

    Object toJson(T o) throws SerializationException;
}

