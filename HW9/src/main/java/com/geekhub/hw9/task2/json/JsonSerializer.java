package com.geekhub.hw9.task2.json;

import com.geekhub.hw9.task2.json.adapter.*;
import org.json.JSONObject;

import java.awt.*;
import java.lang.reflect.Field;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * JsonSerializer converts Java objects to JSON representation.
 */
public class JsonSerializer {

    /**
     * simpleTypes contains java classes for which we should not make any deeper serialization and we should return object as is
     * and use toString() method to get it serialized representation
     */
    private static Set<Class<?>> simpleTypes = Set.of(
            String.class,
            Integer.class,
            Short.class,
            Long.class,
            Byte.class,
            Double.class,
            Float.class,
            Character.class,
            Boolean.class,
            int.class,
            short.class,
            long.class,
            byte.class,
            double.class,
            float.class,
            char.class,
            boolean.class
    );

    /**
     * Main method to convert Java object to JSON. If type of the object is part of the simpleTypes object itself will be returned.
     * If object is null String value "null" will be returned.
     *
     * @param o object to serialize.
     * @return JSON representation of the object.
     */
    public static Object serialize(Object o) throws SerializationException {
        if (null == o) {
            return "null";
        }
        if (simpleTypes.contains(o.getClass())) {
            return o;
        } else {
            try {
                return toJsonObject(o);
            } catch (SerializationException e) {
                throw new SerializationException(e.getMessage(), e);
            } catch (Exception e) {
                throw new SerializationException("Exception during serialization " + o.toString(), e);
            }
        }
    }

    /**
     * Converts Java object to JSON. Uses reflection to access object fields.
     * Uses JsonDataAdapter to serialize complex values. Ignores @Ignore annotated fields.
     *
     * @param o object to serialize to JSON
     * @return JSON object.
     * @throws Exception
     */
    private static JSONObject toJsonObject(Object o) throws Exception {
        JSONObject jsonObject = new JSONObject();

        if (o instanceof Map<?, ?>) {
            jsonObject.put("", ((new MapAdapter()).toJson((Map<?, ?>) o)));
        } else if (o instanceof Collection<?>) {
            jsonObject.put("", ((new CollectionAdapter()).toJson((Collection<?>) o)));
        } else if (o instanceof LocalDate) {
            jsonObject.put("", ((new LocalDateAdapter()).toJson((LocalDate) o)));
        } else if(o instanceof Color) {
            jsonObject.put("", ((new ColorAdapter()).toJson((Color) o)));
        } else {
            Field[] fields = o.getClass().getDeclaredFields();

            for (Field field : fields) {
                if (!field.isAnnotationPresent(Ignore.class)) {
                    field.setAccessible(true);
                    Object object = serialize(field.get(o));
                    if (object instanceof JSONObject) {
                        jsonObject.put(field.getName(), ((JSONObject) object).get("")) ;
                    } else {
                        jsonObject.put(field.getName(), object);
                    }
                }
            }
        }

        return jsonObject;
    }
}

