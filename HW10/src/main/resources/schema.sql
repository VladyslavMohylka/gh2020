create type product as
(
    product_id integer,
    price      numeric,
    quantity   integer
);

alter type product owner to postgres;

create table customers
(
    id         serial      not null
        constraint customers_pk
            primary key,
    first_name varchar(20) not null,
    last_name  varchar(20) not null,
    cell_phone varchar(20) not null
);

alter table customers
    owner to postgres;

create unique index customers_cell_phone_uindex
    on customers (cell_phone);

create table products
(
    id            serial       not null
        constraint products_pk
            primary key,
    name          varchar(20)  not null,
    description   varchar(100) not null,
    current_price numeric      not null
);

alter table products
    owner to postgres;

create table orders
(
    customer_id    integer
        constraint orders_customers_id_fk
            references customers
            on update restrict on delete cascade,
    delivery_place varchar(50)  not null,
    product        hw10.product not null
);

alter table orders
    owner to postgres;


