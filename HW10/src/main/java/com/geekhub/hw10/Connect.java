package com.geekhub.hw10;

import java.sql.*;
import java.util.LinkedHashMap;
import java.util.Map;

public class Connect {
    private Connection conn = null;

    public Connect(String url, String user, String password) {
        try {
            this.conn = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            System.err.printf("Error while get connect with code: %s.", e.getSQLState());
        }
    }

    public boolean addCustomer(String firstName, String lastName, String cellPhone) {
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "INSERT INTO hw10.customers (first_name, last_name, cell_phone) VALUES (?, ?, ?)");
            ps.setString(1, firstName);
            ps.setString(2, lastName);
            ps.setString(3, cellPhone);
            ps.executeUpdate();

            return true;
        } catch (SQLException e) {
            if (e.getSQLState().equals("23505")) {
                System.err.printf("Error in method addCustomer: cellphone %s is already exists.%n", cellPhone);
            } else {
                System.err.printf("Error in method addCustomer with code: %s.%n", e.getSQLState());
            }
            System.err.printf("Cellphone-%s, firstName-%s, lastName-%s.%n", cellPhone, firstName, lastName);

            return false;
        }
    }

    public boolean deleteCustomer(String firstName, String lastName, String cellPhone) {
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "DELETE FROM hw10.customers WHERE first_name = ? AND last_name = ? AND cell_phone = ?");
            ps.setString(1, firstName);
            ps.setString(2, lastName);
            ps.setString(3, cellPhone);

            return ps.executeUpdate() != 0;
        } catch (SQLException e) {
            System.err.printf("Error in method deleteCustomer with code: %s.%n", e.getSQLState());
            System.err.printf("Cellphone-%s, firstName-%s, lastName-%s.%n", cellPhone, firstName, lastName);

            return false;
        }
    }

    public boolean addOrder(int customerId, String deliveryPlace, int productId, int quantity) {
        try {
            double price = getPrice(productId);

            PreparedStatement ps = conn.prepareStatement(
                    "INSERT INTO hw10.orders (customer_id, delivery_place, product) VALUES (?, ?, ?::hw10.product)");
            ps.setInt(1, customerId);
            ps.setString(2, deliveryPlace);
            ps.setString(3, "(" + productId + "," + price + "," + quantity + ")");

            return ps.executeUpdate() != 0;
        } catch (SQLException e) {
            if (e.getSQLState().equals("23503")) {
                System.err.println("Such user does not exist.");
            }
            System.err.printf("Error in method addOrder with code: %s.%n", e.getSQLState());
            System.err.printf("customerId-%s, deliveryPlace-%s, productId-%s.%n", customerId, deliveryPlace, productId);

            return false;
        }
    }

    public boolean deleteOrder(int customerId, String deliveryPlace, int productId) {
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "DELETE FROM hw10.orders WHERE customer_id = ? AND delivery_place = ? AND (product).product_id = ?");
            ps.setInt(1, customerId);
            ps.setString(2, deliveryPlace);
            ps.setInt(3, productId);

            return ps.executeUpdate() != 0;
        } catch (SQLException e) {
            System.err.printf("Error in method deleteOrder with code: %s.%n", e.getSQLState());
            System.err.printf("customerId-%s, deliveryPlace-%s, productId-%s.%n", customerId, deliveryPlace, productId);

            return false;
        }
    }

    public boolean updateOrder(int customerId, String deliveryPlace, int productId, int quantity) {
        try {
            double price = getPrice(productId);

            PreparedStatement ps = conn.prepareStatement(
                    "UPDATE hw10.orders SET product.quantity = ?, product.price = ?" +
                            "WHERE customer_id = ? AND delivery_place = ? AND (product).product_id = ?");
            ps.setInt(1, quantity);
            ps.setDouble(2, price);
            ps.setInt(3, customerId);
            ps.setString(4, deliveryPlace);
            ps.setInt(5, productId);

            return ps.executeUpdate() != 0;
        } catch (SQLException e) {
            System.err.printf("Error in method updateOrder with code: %s.%n", e.getSQLState());
            System.err.printf("customerId-%s, deliveryPlace-%s, productId-%s.%n", customerId, deliveryPlace, productId);

            return false;
        }
    }

    public boolean addQuantityToOrder(int customerId, int productId, int quantity, String deliveryPlace) {
        return updateQuantityToOrder(customerId, productId, quantity, deliveryPlace, '+');
    }

    public boolean subtractQuantityToOrder(int customerId, int productId, int quantity, String deliveryPlace) {
        return updateQuantityToOrder(customerId, productId, quantity, deliveryPlace, '-');
    }

    private boolean updateQuantityToOrder(int customerId, int productId, int quantity, String deliveryPlace, char plusMinus) {
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "UPDATE hw10.orders SET product.quantity = (" +
                            "(SELECT (a.product).quantity FROM hw10.orders AS a WHERE customer_id = ? AND (a.product).product_id = ? AND delivery_place = ?)" + plusMinus + "?)" +
                            " WHERE customer_id = ? AND delivery_place = ? AND (product).product_id = ?");
            ps.setInt(1, customerId);
            ps.setInt(2, productId);
            ps.setString(3, deliveryPlace);
            ps.setInt(4, quantity);
            ps.setInt(5, customerId);
            ps.setString(6, deliveryPlace);
            ps.setInt(7, productId);

            return ps.executeUpdate() != 0;
        } catch (SQLException e) {
            System.err.printf("Error while update quantity product from order with code: %s.%n", e.getSQLState());
            System.err.printf("customerId-%s, deliveryPlace-%s, productId-%s.%n", customerId, deliveryPlace, productId);

            return false;
        }
    }

    public boolean addProduct(String name, String description, double price) {
        try {
            if (price <= 0) {
                throw new IllegalArgumentException();
            }
            PreparedStatement ps = conn.prepareStatement(
                    "INSERT INTO hw10.products (name, description, current_price) VALUES (?, ?, ?)");
            ps.setString(1, name);
            ps.setString(2, description);
            ps.setDouble(3, price);

            return ps.executeUpdate() != 0;
        } catch (SQLException e) {
            System.err.printf("Error in method addProduct with code: %s.%n", e.getSQLState());
        } catch (IllegalArgumentException e) {
            System.err.println("Error in method addProduct, illegal argument: price.");
        }
        System.err.printf("name-%s, price-%s.%n", name, price);

        return false;
    }

    public boolean deleteProduct(String name, double price) {
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "DELETE FROM hw10.products WHERE name = ? AND current_price = ?");
            ps.setString(1, name);
            ps.setDouble(2, price);

            return ps.executeUpdate() != 0;
        } catch (SQLException e) {
            System.err.printf("Error in method deleteProduct with code: %s.%n", e.getSQLState());
            System.err.printf("name-%s, price-%s.%n", name, price);

            return false;
        }
    }

    public boolean updateProduct(int productId, String description, int price) {
        try {
            if (price <= 0) {
                throw new IllegalArgumentException();
            }
            PreparedStatement ps;

            if (description == null) {
                ps = conn.prepareStatement(
                        "UPDATE hw10.products SET current_price = ? WHERE id = ?");
                ps.setInt(1, price);
                ps.setInt(2, productId);
            } else {
                ps = conn.prepareStatement(
                        "UPDATE hw10.products SET current_price = ?, description = ? WHERE id = ?");
                ps.setInt(1, price);
                ps.setString(2, description);
                ps.setInt(3, productId);
            }

            return ps.executeUpdate() != 0;
        } catch (SQLException e) {
            System.err.printf("Error in method updateProduct with code: %s.%n", e.getSQLState());
        } catch (IllegalArgumentException e) {
            System.err.println("Error in method updateProduct, illegal argument: price");
        }
        System.err.printf("productId-%s, price-%s.%n", productId, price);

        return false;
    }

    public int getCustomerId(String cellphone) {
        int id = -1;

        try {
            PreparedStatement ps = conn.prepareStatement(
                    "SELECT id FROM hw10.customers WHERE cell_phone = ?");
            ps.setString(1, cellphone);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                id = rs.getInt("id");
            }
        } catch (SQLException e) {
            System.out.println("Error in method getCustomerId with code :" + e.getSQLState());
            System.err.printf("cellphone-%s.%n", cellphone);
        }

        return id;
    }

    public int getProductId(String name, double price) {
        int id = -1;

        try {
            PreparedStatement ps = conn.prepareStatement(
                    "SELECT id FROM hw10.products WHERE name = ? AND current_price = ?");
            ps.setString(1, name);
            ps.setDouble(2, price);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                id = rs.getInt("id");
            }
        } catch (SQLException e) {
            System.out.println("Error in method getProductId with code :" + e.getSQLState());
            System.err.printf("name-%s, price-%s.%n", name, price);
        }

        return id;
    }

    private double getPrice(int id) {
        double price = 0;

        try {
            PreparedStatement ps = conn.prepareStatement(
                    "SELECT current_price FROM hw10.products WHERE id = ?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                price = rs.getDouble(1);
            }
        } catch (SQLException e) {
            System.out.println("Error in method getPce with code :" + e.getSQLState());
            System.err.printf("productId-%s.%n", price);
        }

        return price;
    }

    public Map<Integer, Integer> getClientsSpentMoney() {
        Map<Integer, Integer> result =  new LinkedHashMap<>();

        try {
            PreparedStatement ps = conn.prepareStatement(
                    "SELECT a.id, SUM ((b.product).price * (b.product).quantity) FROM hw10.customers AS a LEFT JOIN hw10.orders AS b ON a.id = b.customer_id GROUP BY a.id");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                result.put(rs.getInt(1), rs.getInt(2));
            }
        } catch (SQLException e) {
            System.err.printf("Error in method getClientSpentMoney with code: %s.%n", e.getSQLState());
        }

        return result;
    }

    public String getMostPopular() {
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "SELECT (a.product).product_id FROM hw10.orders AS a GROUP BY (a.product).product_id ORDER BY SUM((a.product).quantity) DESC");
            ResultSet rs = ps.executeQuery();
            if(rs.next()) {
              return rs.getString(1);
            }
        } catch (SQLException e) {
            System.err.printf("Error in method updateProduct with code: %s.%n", e.getSQLState());
        }

        return "";
    }
}
