package com.geekhub.hw10;

import java.sql.SQLException;

public class Application {
    public static void main(String[] args) throws SQLException {
        String url = "jdbc:postgresql://localhost:5432/postgres";
        String user = "postgres";
        String password = "1";
        Connect db = new Connect(url, user, password);

        // System.out.println(db.getCustomerId("064"));
        // System.out.println(db.getProductId("orange", 22));

        // db.addCustomer("Tom", "Robins", "111");
        // db.addOrder(23, "Cherkasy", 6,56);
        // db.addOrder(23, "Kiev", 6,56);
        // db.addProduct("Butter", "some description", 14.59);

        // System.out.println(db.deleteCustomer("Tom", "Robins", "067"));
        // db.deleteOrder(23, "Kiev", 6);
        // db.deleteProduct("orange", 40.23);

        // db.addQuantityToOrder(23, 6, 11, "Kiev");
        // db.subtractQuantityToOrder(23, 6, 10, "Kiev");

        // db.updateOrder(23, "Cherkasy", 6, 11);
        // db.updateProduct(5, null, 32);

       /* for (Map.Entry<Integer, Integer> entry : db.getClientsSpentMoney().entrySet()) {
            System.out.println("User id: " + entry.getKey() + ", spent money: " + entry.getValue());
        }*/
        // System.out.println("Id most populars product: " + db.getMostPopular());
    }
}
