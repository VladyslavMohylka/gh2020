package com.geekhub.hw2.abstarct;

import java.text.DecimalFormat;

public class ConsoleOutput {
    DecimalFormat df = new DecimalFormat("###.##");

    public void inFigureName() {
        System.out.println("enter figure name: ");
    }

    public void inRadius() {
        System.out.print("enter radius: ");
    }

    public void inA() {
        System.out.print("enter side a: ");
    }

    public void inB() {
        System.out.print("enter side b: ");
    }

    public void inC() {
        System.out.print("enter side c: ");
    }

    public void outPerimeter(String figure, double value) {
        System.out.println(figure.toLowerCase() + " perimeter: " + df.format(value));
    }

    public void outSquare(String figure, double value) {
        System.out.println(figure.toLowerCase() + " square: " + df.format(value));
    }
}
