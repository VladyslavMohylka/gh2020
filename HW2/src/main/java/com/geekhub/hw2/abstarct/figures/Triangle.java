package com.geekhub.hw2.abstarct.figures;

import com.geekhub.hw2.abstarct.Shape;

public class Triangle extends Shape {
    final double a;
    final double b;
    final double c;
    final double hP;

    public Triangle(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.hP = (a + b + c)/2;
    }

    public double calculateArea() {
        return Math.sqrt(hP * (hP - a) * (hP - b) * (hP - c));
    }

    public double calculatePerimeter() {
        return a + b + c;
    }
}
