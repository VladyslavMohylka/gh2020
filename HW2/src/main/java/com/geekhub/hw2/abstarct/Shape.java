package com.geekhub.hw2.abstarct;

public abstract class Shape {
    public abstract double calculateArea();

    public abstract double calculatePerimeter();
}
