package com.geekhub.hw2.abstarct.figures;

import com.geekhub.hw2.abstarct.Shape;

public class Rectangle extends Shape {
    final double a;
    final double b;

    public Rectangle(double a, double b) {
        this.a = a;
        this.b = b;
    }

    public double calculateArea() {
        return a * b;
    }

    public double calculatePerimeter() {
        return a * 2 + b * 2;
    }
}
