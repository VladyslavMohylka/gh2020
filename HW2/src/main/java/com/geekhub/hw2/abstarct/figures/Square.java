package com.geekhub.hw2.abstarct.figures;

import com.geekhub.hw2.abstarct.Shape;

public class Square extends Shape {
    final double a;

    public Square(double a) {
        this.a = a;
    }

    public double calculateArea() {
        return Math.pow(a, 2);
    }

    public double calculatePerimeter() {
        return a * 4;
    }
}
