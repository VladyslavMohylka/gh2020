package com.geekhub.hw2.abstarct;

import com.geekhub.hw2.abstarct.figures.*;
import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        ConsoleOutput consoleOutput = new ConsoleOutput();
        Scanner input = new Scanner(System.in);
        String createdShape;

        consoleOutput.inFigureName();
        createdShape = input.next().toUpperCase();
        switch (Figure.valueOf(createdShape)) {
            case CIRCLE: {
                double radius;

                consoleOutput.inRadius();
                radius = input.nextDouble();

                Circle circle = new Circle(radius);
                consoleOutput.outPerimeter(createdShape, circle.calculatePerimeter());
                consoleOutput.outSquare(createdShape, circle.calculateArea());
                break;
            }
            case SQUARE: {
                double a;
                double diagonal;

                consoleOutput.inA();
                a = input.nextDouble();

                Square square = new Square(a);
                diagonal = Math.sqrt(2) * a;
                consoleOutput.outPerimeter(createdShape, square.calculatePerimeter());
                consoleOutput.outSquare(createdShape, square.calculateArea());

                Triangle triangle = new Triangle(a, a, diagonal);
                consoleOutput.outPerimeter("triangle", triangle.calculatePerimeter());
                consoleOutput.outSquare("triangle", triangle.calculateArea());
                break;
            }
            case TRIANGLE: {
                double a;
                double b;
                double c;

                consoleOutput.inA();
                a = input.nextDouble();
                consoleOutput.inB();
                b = input.nextDouble();
                consoleOutput.inC();
                c = input.nextDouble();

                Triangle triangle = new Triangle(a, b, c);
                consoleOutput.outPerimeter(createdShape, triangle.calculatePerimeter());
                consoleOutput.outSquare(createdShape, triangle.calculateArea());
                break;
            }
            case RECTANGLE: {
                double a;
                double b;
                double diagonal;

                consoleOutput.inA();
                a = input.nextDouble();
                consoleOutput.inB();
                b = input.nextDouble();

                Rectangle rectangle = new Rectangle(a, b);
                diagonal = Math.sqrt(a * a + b * b);
                consoleOutput.outPerimeter(createdShape, rectangle.calculatePerimeter());
                consoleOutput.outSquare(createdShape, rectangle.calculateArea());

                Triangle triangle = new Triangle(a, b, diagonal);
                consoleOutput.outPerimeter("triangle", triangle.calculatePerimeter());
                consoleOutput.outSquare("triangle", triangle.calculateArea());
                break;
            }
        }
    }
}
