package com.geekhub.hw2.interfase.figures;

import com.geekhub.hw2.interfase.Shape;

public class Rectangle implements Shape {
    final double a;
    final double b;

    public Rectangle(double a, double b) {
        this.a = a;
        this.b = b;
    }

    public double calculateArea() {
        return a * b;
    }

    public double calculatePerimeter() {
        return a * 2 + b * 2;
    }
}
