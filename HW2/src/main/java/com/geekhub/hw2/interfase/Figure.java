package com.geekhub.hw2.interfase;

public enum Figure {
    CIRCLE,
    SQUARE,
    RECTANGLE,
    TRIANGLE
}
