package com.geekhub.hw2.interfase.figures;

import com.geekhub.hw2.interfase.Shape;

public class Square implements Shape {
    final double a;

    public Square(double a) {
        this.a = a;
    }

    public double calculateArea() {
        return Math.pow(a, 2);
    }

    public double calculatePerimeter() {
        return a * 4;
    }
}
