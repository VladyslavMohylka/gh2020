package com.geekhub.hw2.interfase;

public interface Shape {
   double calculateArea ();

   double calculatePerimeter ();
}
