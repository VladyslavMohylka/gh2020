package com.geekhub.hw2.interfase.figures;

import com.geekhub.hw2.interfase.Shape;

public class Circle implements Shape {
    final double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    public double calculateArea() {
        return Math.PI * Math.pow(radius, 2);
    }

    public double calculatePerimeter() {
        return 2 * Math.PI * radius;
    }
}
