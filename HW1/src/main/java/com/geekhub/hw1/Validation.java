package com.geekhub.hw1;

public class Validation {

    public boolean validationNumb(String phoneNumb) {
        String[] mobileIndex = {"093", "063", "073", "067", "068", "096", "097", "098", "050", "066", "095", "099"};

        if (phoneNumb.length() != 10) {
            return false;
        } else {
            return indexConformance(mobileIndex, phoneNumb);
        }
    }

    private boolean indexConformance(String[] indexArr, String phone) {
        boolean result = false;

        for (String index : indexArr) {
            if (phone.startsWith(index)) {
                result = true;
                break;
            }
        }

        return result;
    }
}