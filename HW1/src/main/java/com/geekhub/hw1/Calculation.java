package com.geekhub.hw1;

public class Calculation {

    public int getSumOfDigits(int value) {
        int sum = 0;

        while (value > 10) {
            int leftover = value % 10;
            sum += leftover;
            value /= 10;
        }
        sum += value;

        return sum;
    }
}

