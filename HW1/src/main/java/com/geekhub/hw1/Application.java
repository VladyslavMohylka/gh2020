package com.geekhub.hw1;

public class Application {

    public static void main(String[] args) {
        Validation validator = new Validation();
        Calculation calculation = new Calculation();
        ConsoleInput consoleInput = new ConsoleInput();
        ConsoleOutput consoleOutput = new ConsoleOutput();
        String phoneNumber = "empty";
        int roundOfCalculation = 1;
        int sum = 0;

        do {
            if (!phoneNumber.equals("empty")) {
                consoleOutput.incorrectPhNum();
            }
            consoleOutput.enterPhNum();
            phoneNumber = consoleInput.enterPhNum();
            phoneNumber = phoneNumber.replaceAll("\\D","");
        } while (!validator.validationNumb(phoneNumber));

        do {
            if (sum == 0) {
                sum = calculation.getSumOfDigits(Integer.parseInt(phoneNumber));
            } else {
                sum = calculation.getSumOfDigits(sum);
            }
            consoleOutput.roundResult(roundOfCalculation, sum);
            roundOfCalculation++;
        } while (sum > 9);

        consoleOutput.finalResult(sum);
    }
}
