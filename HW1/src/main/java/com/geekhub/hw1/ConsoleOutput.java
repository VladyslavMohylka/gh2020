package com.geekhub.hw1;

public class ConsoleOutput {

    public void incorrectPhNum() {
        System.out.println("wrong number, try again");
    }

    public void enterPhNum() {
        System.out.print("enter you phone number : +38");
    }

    public void roundResult(int round, long sum) {
        System.out.println(round + " round of calculation, sum is: " + sum);
    }

    public void finalResult(int value) {
        switch (value) {
        case 1:
            System.out.println("Final result is : One");
            break;
        case 2:
            System.out.println("Final result is : Two");
            break;
        case 3:
            System.out.println("Final result is : Three");
            break;
        case 4:
            System.out.println("Final result is : Four");
            break;
        default:
            System.out.println("Final result is : " + value);
            break;
        }
    }
}
