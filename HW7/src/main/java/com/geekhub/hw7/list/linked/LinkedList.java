package com.geekhub.hw7.list.linked;

import com.geekhub.hw7.list.List;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;

public class LinkedList<E> implements List<E> {
    private Node<E> head;
    private Node<E> tail;
    private int size = 0;

    public LinkedList() {
    }

    /* Only for tests */
    @SafeVarargs
    public LinkedList(E ... elements) {
        for (E element : elements) {
            add(element);
        }
    }

    @Override
    public boolean add(E element) {
        Node<E> node = new Node<>(element, null);

        if (tail == null) {
            head = node;
        } else {
            tail.next = node;
        }
        tail = node;
        size++;

        return true;
    }

    @Override
    public boolean add(int index, E element) {
        if (!isIndexCorrect(index)) {
            return false;
        }
        Node<E> current;
        Node<E> next;
        Node<E> newNode;

        if (isIndexAtFirst(index)) {
            newNode = new Node<>(element, head);
            head = newNode;
        } else if (isIndexAtMiddle(index)) {
            current = getNode(index);
            next = current.next;
            newNode = new Node<>(element, next);
            current.next = newNode;
        } else if (isIndexAtLast(index)) {
            newNode = new Node<>(element, null);
            tail.next = newNode;
        }
        size++;

        return true;
    }

    @Override
    public boolean addAll(List<E> elements) {
        for (E element : elements) {
            add(element);
        }

        return true;
    }

    @Override
    public boolean addAll(int index, List<E> elements) {
        if (!isIndexCorrect(index) || elements.size() == 0) {
            return false;
        }
        Node<E> current = null;
        Node<E> next = null;

        if (isIndexAtFirst(index)) {
            next = head;
        } else if (isIndexAtMiddle(index)) {
            current = getNode(index);
            next = current.next;
        } else {
            current = tail;
        }
        for (E element : elements) {
            Node<E> newNode = new Node<>(element, next);

            if (current == null) {
                current = newNode;
                next = newNode.next;
                head = newNode;
            } else if (next == null) {
                current.next = newNode;
                current = newNode;
                tail = newNode;
            } else {
                current.next = newNode;
                current = newNode;
            }
            size++;
        }

        return true;
    }

    @Override
    public boolean clear() {
        this.head = this.tail = null;
        this.size = 0;

        return true;
    }

    @Override
    public E remove(int index) {
        if (!isIndexCorrect(index)) {
            return null;
        }
        Node<E> current = getNode(index);
        Node<E> next = current.next;
        E deletedElement = next.element;
        int indexEqualsSize = index + 1;

        if (isIndexAtFirst(index)) {
            deletedElement = current.element;
            head = next;
        } else if (isIndexAtMiddle(indexEqualsSize)) {
            current.next = next.next;
        } else {
            current.next = null;
            tail = current;
        }
        size--;

       return deletedElement;
    }

    private Node<E> getNode(int index) {
        if (!isIndexCorrect(index)) {
            return null;
        }
        Node<E> result = head;

        for (int i = 1; i < index; i++) {
            result = result.next;
        }

        return result;
    }

    private boolean isIndexAtFirst(int index) {
        return index == 0;
    }

    private boolean isIndexAtMiddle(int index) {
        return 0 < index && index < size;
    }

    private boolean isIndexAtLast(int index) {
        return index == size;
    }

    @Override
    public E remove(E element) {
        E removedElement = null;
        Node<E> current = head;
        Node<E> next = current.next;

        if (current.element.equals(element)) {
            removedElement = current.element;
            head = current.next;
        } else {
            while (current.next != null) {
                if (element.equals(next.element)) {
                    current.next = next.next;
                    removedElement = next.element;
                    break;
                }
                current = current.next;
            }
        }
        size--;

        return removedElement;
    }

    @Override
    public E get(int index) {
        if (!isIndexCorrect(index)) {
            return null;
        }
        Iterator<E> iterator = iterator();
        int count = 0;
        E element = null;

        while (count <= index) {
            count++;
            element = iterator.next();
        }

        return element;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0 && tail == null;
    }

    @Override
    public int indexOf(E element) {
        Iterator<E> it = iterator();
        int count = 0;

        while (it.hasNext()) {
            if (element.equals(it.next())) {
                return count;
            }
            count++;
        }

        return -1;
    }

    @Override
    public boolean contains(E element) {
        return indexOf(element) >= 0;
    }

    @Override
    public Iterator<E> iterator() {
        return new LinkedListIterator<>(head);
    }

    @Override
    public String toString() {
        Iterator<E> it = iterator();
        if (!it.hasNext()) {
            return "[]";
        }

        StringBuilder str = new StringBuilder("{");
        str.append(it.next().toString());
        while (it.hasNext()) {
            E element = it.next();
            str.append(", ").append(element.toString());
        }

        return str.append("}").toString();
    }

    @Override
    public List<E> filter(Predicate<E> predicate) {
        Iterator<E> it = iterator();
        List<E> result = new LinkedList<>();

        while(it.hasNext()) {
            E next = it.next();

            if (predicate.test(next)) {
                result.add(next);
            }
        }

        return result;
    }

    @Override
    public <T> List<T> map(Function<E, T> mapper) {
        Iterator<E> it = iterator();
        List<T> result = new LinkedList<>();

        while (it.hasNext()) {
           result.add(mapper.apply(it.next()));
        }

        return result;
    }

    @Override
    public Optional<E> max(Comparator<E> comparator) {
        Iterator<E> it = iterator();
        E result = it.next();

        while (it.hasNext()) {
            E next = it.next();

            if (comparator.compare(result, next) < 0) {
                result = next;
            }
        }

        return Optional.ofNullable(result);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof LinkedList)) {
            return false;
        }
        if (size != ((LinkedList<?>) obj).size()) {
            return false;
        }
        Iterator<E> e1 = iterator();
        Iterator<?> e2 = ((LinkedList<?>) obj).iterator();

        while (e1.hasNext()) {
            E o1 = e1.next();
            Object o2 = e2.next();

            if (!(o1.equals(o2))) {
                return false;
            }
        }

        return true;
    }

    private boolean isIndexCorrect(int index) {
        return 0 <= index && index <= size;
    }
}

