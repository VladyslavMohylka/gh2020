package com.geekhub.hw7.list.linked;

import java.util.Iterator;

public class LinkedListIterator<E> implements Iterator<E> {
    private Node<E> head;

    LinkedListIterator(Node<E> head) {
        this.head = head;
    }

    @Override
    public boolean hasNext() {
        return head != null;
    }

    @Override
    public E next() {
        if (!hasNext()) {
            return null;
        }

        E next = head.element;
        head = head.next;

        return  next;
    }
}
