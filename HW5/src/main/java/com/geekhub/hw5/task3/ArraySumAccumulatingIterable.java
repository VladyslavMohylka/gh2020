package com.geekhub.hw5.task3;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArraySumAccumulatingIterable implements Iterable<Integer> {
    private final int[] array;

    public ArraySumAccumulatingIterable(int[] array) {
        this.array = array;
    }

    public Iterator<Integer> iterator() {
        return new IteratorImpl(array);
    }

    private final static class IteratorImpl implements Iterator<Integer> {
        private final int[] array;
        private int sum = 0;
        private int index = 0;

        public IteratorImpl(int[] array) {
            this.array = array;
        }

        public boolean hasNext() {
            if (array.length == 0) {
                return false;
            }

            return index < array.length;
        }

        public Integer next() {
            int element;

            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            element = array[index];
            index = index + 1;

            return sum += element;
        }
    }
}
