package com.geekhub.hw5.task3;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class OnlyOddIterable implements Iterable<Integer> {
    private final int[] array;

    public OnlyOddIterable(int[] array) {
        this.array = array;
    }

    public Iterator<Integer> iterator() {
        return new IteratorImpl(array);
    }

    private final static class IteratorImpl implements Iterator<Integer> {
        private final int[] array;
        private int index = 0;

        public IteratorImpl(int[] array) {
            this.array = array;
        }

        public boolean hasNext() {
            if (array.length == 0) {
                return false;
            }

            while (isPairCurrentNumber()) {
                index = index + 1;
                if (index >= array.length) {
                    return false;
                }
            }

            return true;
        }

        private boolean isPairCurrentNumber() {
            return array[index] % 2 == 0;
        }

        public Integer next() {
            int element;

            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            element = array[index];
            index = index + 1;

            return element;
        }
    }
}
