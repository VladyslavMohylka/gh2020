package com.geekhub.hw5.task2;

public class Person implements Comparable<Person> {
    private final String name;
    private final Integer age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }

    public int compareTo(Person o) {
        return name.compareTo(o.name);
    }

    public String toString() {
        return "[name = " + name + ", age = " + age + "]";
    }
}
