package com.geekhub.hw5.task2.comparators;

import com.geekhub.hw5.task2.Person;
import java.util.Comparator;

public class PersonNameThenAgeComparator implements Comparator<Person> {
    public int compare(Person o1, Person o2) {
        int result = o1.getName().compareTo(o2.getName());

        if (result == 0) {
            result = o1.getAge().compareTo(o2.getAge());
        }

        return result;
    }
}
