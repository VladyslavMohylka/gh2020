package com.geekhub.hw5.task2.sorters;

import com.geekhub.hw5.task2.Direction;
import java.util.Comparator;

public class BubbleSortArraySorter implements ArraySorter {
    public <T extends Comparable<T>> void sort(T[] array, Direction direction) {
        boolean sorted = false;

        while (!sorted) {
            sorted = true;
            for (int j = 0; j < array.length - 1; j++) {
                if (array[j].compareTo(array[j + 1]) * direction.getDirection() > 0) {
                    swap(array, j, j + 1);
                    sorted = false;
                }
            }
        }
    }

    public <T> void sort(T[] array, Comparator<T> comparator, Direction direction) {
        boolean sorted = false;

        while (!sorted) {
            sorted = true;
            for (int j = 0; j < array.length - 1; j++) {
                if (comparator.compare(array[j], array[j + 1]) * direction.getDirection() > 0) {
                    swap(array, j, j + 1);
                    sorted = false;
                }
            }
        }
    }

    private <T> void swap (T[] a, int x, int y) {
        T t = a[x];
        a[x] = a[y];
        a[y] = t;
    }
}
