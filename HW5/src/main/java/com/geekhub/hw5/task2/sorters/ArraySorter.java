package com.geekhub.hw5.task2.sorters;

import com.geekhub.hw5.task2.Direction;
import java.util.Comparator;

public interface ArraySorter {
    <T extends Comparable<T>> void sort(T[] array, Direction direction);

    <T> void sort(T[] array, Comparator<T> comparator, Direction direction);
}
