package com.geekhub.hw5.task2;

import com.geekhub.hw5.task2.sorters.*;
import com.geekhub.hw5.task2.comparators.*;
import java.util.Arrays;

public class Application {
    public static void main(String[] args) {
        InsertionSortArraySorter insertionSorter = new InsertionSortArraySorter();
        BubbleSortArraySorter bubbleSorter = new BubbleSortArraySorter();
        Person[] personArray = new Person[5];

        personArray[0] = new Person("Tom", 30);
        personArray[1] = new Person("Jerry", 29);
        personArray[2] = new Person("Jerry", 32);
        personArray[3] = new Person("Andy", 32);
        personArray[4] = new Person("Zakaria", 29);

        //insertionSorter.sort(personArray, Direction.DESC);
        insertionSorter.sort(personArray, new PersonAgeThenNameComparator(), Direction.ASC);
        //bubbleSorter.sort(personArray, new PersonNameThenAgeComparator(), Direction.ASC);
        //bubbleSorter.sort(personArray, Direction.DESC);
        System.out.println(Arrays.toString(personArray));
    }
}
