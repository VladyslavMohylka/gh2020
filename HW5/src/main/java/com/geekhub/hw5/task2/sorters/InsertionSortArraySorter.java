package com.geekhub.hw5.task2.sorters;

import com.geekhub.hw5.task2.Direction;
import java.util.Comparator;

public class InsertionSortArraySorter implements ArraySorter {
    public <T extends Comparable<T>> void sort(T[] array, Direction direction) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i; j >= 0; j--) {
                if (array[j].compareTo(array[j + 1]) * direction.getDirection() > 0) {
                    swap(array, j, j + 1);
                } else {
                    break;
                }
            }
        }
    }

    public <T> void sort(T[] array, Comparator<T> comparator, Direction direction) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i; j >= 0; j--) {
                if (comparator.compare(array[j], array[j + 1]) * direction.getDirection() > 0) {
                    swap(array, j, j + 1);
                } else {
                    break;
                }
            }
        }
    }

    private <T> void swap (T[] a, int x, int y) {
        T t = a[x];
        a[x] = a[y];
        a[y] = t;
    }
}
