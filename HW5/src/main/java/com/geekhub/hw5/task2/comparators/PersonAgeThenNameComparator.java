package com.geekhub.hw5.task2.comparators;

import com.geekhub.hw5.task2.Person;
import java.util.Comparator;

public class PersonAgeThenNameComparator implements Comparator<Person> {
    public int compare(Person o1, Person o2) {
        int result = o1.getAge().compareTo(o2.getAge());

        if (result == 0) {
            result = o1.getName().compareTo(o2.getName());
        }

        return result;
    }
}
