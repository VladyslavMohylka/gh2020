package com.geekhub.hw5.task1.products.milky;

import com.geekhub.hw5.task1.products.Product;

public class Butter extends Product {
    public Butter(double price, String name, double quantity) {
        super(price, name, quantity);
    }
}
