package com.geekhub.hw5.task1.inventory;

import com.geekhub.hw5.task1.products.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

public class Inventory<T extends Product> {
    private final List<T> products = new ArrayList<>();

    public void addProduct(T product) {
        products.add(product);
    }

    public double getSumAllProducts() {
        double sum = 0;
        Iterator<T> iterator = products.iterator();

        while (iterator.hasNext()) {
            T next = iterator.next();
            sum += next.getPrice() * next.getQuantity();
        }

        return sum;
    }
}
