package com.geekhub.hw5.task1;

import com.geekhub.hw5.task1.inventory.Inventory;
import com.geekhub.hw5.task1.products.Product;
import com.geekhub.hw5.task1.products.meals.Bread;
import com.geekhub.hw5.task1.products.milky.*;

public class Application {

    public static void main(String[] args) {
        Inventory<Product> inventory = new Inventory<>();

        inventory.addProduct(new Bread(24, "bread1", 11));
        inventory.addProduct(new Bread(20, "bread2", 10));
        inventory.addProduct(new Bread(17, "bread3", 16));
        inventory.addProduct(new Milk(17, "milk1", 15));
        inventory.addProduct(new Butter(34, "butter1", 12));

        System.out.println(inventory.getSumAllProducts());
    }
}
