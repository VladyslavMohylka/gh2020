package com.geekhub.hw5.task1.products.milky;

import com.geekhub.hw5.task1.products.Product;

public class Milk extends Product {
    public Milk(double price, String name, double quantity) {
        super(price, name, quantity);
    }
}
