package com.geekhub.hw5.task1.products.meals;

import com.geekhub.hw5.task1.products.Product;

public class Bread extends Product {
    public Bread(double price, String name, double quantity) {
        super(price, name, quantity);
    }
}
