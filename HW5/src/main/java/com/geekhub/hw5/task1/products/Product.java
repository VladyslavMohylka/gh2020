package com.geekhub.hw5.task1.products;

public class Product {
    private final double price;
    private final String name;
    private final double quantity;

    public Product(double price, String name, double quantity) {
        this.price = price;
        this.name = name;
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public double getQuantity() {
        return quantity;
    }
}
