package com.geekhub.hw6.task1;

import java.util.*;
import com.geekhub.hw6.task1.person.Person;

public class Application {
    public static void main(String[] args) {
        List<Integer> listEmpty = new ArrayList<>();
        List<Integer> listI = Arrays.asList(1, 2, 11, 4, 4, 2, 5, 6, 7);
        List<Person> listP = Arrays.asList(new Person("Tomas", 29),
                                           new Person("Bob", 30),
                                           new Person("Robin", 31),
                                           new Person("Dinaria", 29),
                                           new Person("Robin", 31),
                                           new Person("Bob", 32));

        //System.out.println(StreamLikeOps.generate(() -> (int) (Math.random() * 100), ArrayList::new, 9));

        //System.out.println(StreamLikeOps.filter(listI, n -> n > 3));
        //System.out.println(StreamLikeOps.filter(listP, n -> n.getName().length() > 3));

        //System.out.println(StreamLikeOps.anyMatch(listEmpty, n -> n >= 1));
        //System.out.println(StreamLikeOps.anyMatch(listI, n -> n == 11));
        //System.out.println(StreamLikeOps.anyMatch(listP, n -> n.getAge() < 11));

        //System.out.println(StreamLikeOps.allMatch(listI, n -> n >= 1));
        //System.out.println(StreamLikeOps.allMatch(listEmpty, n -> n >= 1));
        //System.out.println(StreamLikeOps.allMatch(listP, n -> n.getName() != null));

        //System.out.println(StreamLikeOps.noneMatch(listEmpty, n -> n >= 11));
        //System.out.println(StreamLikeOps.noneMatch(listI, n -> n >= 21));

        //System.out.println(StreamLikeOps.map(listI, (r) -> r + 1, ArrayList::new));
        //System.out.println(StreamLikeOps.map(listP, Person::getName, ArrayList::new));

        //System.out.println(StreamLikeOps.max(listEmpty, Integer::compare));
        //System.out.println(StreamLikeOps.max(listI, Integer::compare));
        //System.out.println(StreamLikeOps.max(listP, Comparator.comparing(Person::getAge)));
        //System.out.println(StreamLikeOps.max(listP, Comparator.comparing(Person::getName)));

        //System.out.println(StreamLikeOps.min(listI, Integer::compare));
        //System.out.println(StreamLikeOps.min(listP, Comparator.comparing(Person::getAge)));

        //System.out.println(StreamLikeOps.distinct(listI, ArrayList::new));
        //System.out.println(StreamLikeOps.distinct(listP, ArrayList::new));

        //StreamLikeOps.forEach(listP, (Person p) -> System.out.println(p.getName()));

        //System.out.println(StreamLikeOps.reduce(listI, Integer::sum));
        //System.out.println(StreamLikeOps.reduce(listEmpty, Integer::sum));

        //System.out.println(StreamLikeOps.reduce(10, listI, Integer::sum));
/*
        System.out.println(StreamLikeOps.partitionBy(listP,
                                                     (e) -> e.getAge() > 30,
                                                     () -> new TreeMap<>(),
                                                     () -> new ArrayList<>()));*/
/*
        System.out.println(StreamLikeOps.groupBy(listEmpty,
                                                 (e) -> e > 3 ,
                                                 () -> new HashMap<>(),
                                                 ArrayList::new));*/
 /*       System.out.println(StreamLikeOps.groupBy(listP,
                                                 Person::getName,
                                                 () -> new TreeMap<>(),
                                                 ArrayList::new));*/
/*
        System.out.println(StreamLikeOps.toMap(listP,
                                               (e) -> e.getName() ,
                                               (e) -> Integer.toString(e.getAge()) ,
                                               (oldVal, newVal) -> oldVal + "; " + newVal,
                                               () -> new TreeMap<String, String>()));*/
/*
        System.out.println(StreamLikeOps.partitionByAndMapElement(listP,
                                                                  (e) -> e.getAge() > 30,
                                                                  ()-> new HashMap<>(),
                                                                  ArrayList::new,
                                                                  Person::getName));*/

/*        System.out.println(StreamLikeOps.groupByAndMapElement(listP,
                                                              (e) -> e.getName().length(),
                                                              ()-> new HashMap<>(),
                                                              ArrayList::new,
                                                              Person::getName));*/
    }
}
