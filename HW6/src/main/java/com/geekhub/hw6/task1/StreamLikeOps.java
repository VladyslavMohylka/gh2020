package com.geekhub.hw6.task1;

import java.util.*;
import java.util.function.*;

public class StreamLikeOps {
    private StreamLikeOps() {
    }

    public static <E> List<E> generate(Supplier<E> generator,
                                       Supplier<List<E>> listFactory,
                                       int count) {
        List<E> result = listFactory.get();

        for (int i = 0; i < count; i++) {
            result.add(generator.get());
        }

        return result;
    }

    public static <E> List<E> filter(List<E> elements, Predicate<E> filter) {
        List<E> result = new ArrayList<>();

        for (E element : elements) {
            if (filter.test(element)) {
                result.add(element);
            }
        }

        return result;
    }

    public static <E> boolean anyMatch(List<E> elements, Predicate<E> predicate) {
        return !elements.isEmpty() && !noneMatch(elements, predicate);
    }

    public static <E> boolean noneMatch(List<E> elements, Predicate<E> predicate) {
        for (E element : elements) {
            if (predicate.test(element)) {
                return false;
            }
        }

        return !elements.isEmpty();
    }

    public static <E> boolean allMatch(List<E> elements, Predicate<E> predicate) {
        for (E element : elements) {
            if (!predicate.test(element)) {
                return false;
            }
        }

        return !elements.isEmpty();
    }

    public static <T, R> List<R> map(List<T> elements,
                                     Function<T, R> mappingFunction,
                                     Supplier<List<R>> listFactory) {
        List<R> result = listFactory.get();

        for (T element : elements) {
            result.add(mappingFunction.apply(element));
        }

        return result;
    }

    public static <E> Optional<E> max(List<E> elements, Comparator<E> comparator) {
        Iterator<E> iterator = elements.iterator();
        E current;

        if (!iterator.hasNext()) {
            return Optional.empty();
        }
        current = iterator.next();

        for (E element : elements.subList(1, elements.size())) {
            if (comparator.compare(current, element) < 0) {
                current = element;
            }
        }

        return Optional.of(current);
    }

    public static <E> Optional<E> min(List<E> elements, Comparator<E> comparator) {
        return max(elements, comparator.reversed());
    }

    public static <E> List<E> distinct(List<E> elements, Supplier<List<E>> listFactory) {
        List<E> result = listFactory.get();
        Set<E> set = new HashSet<>();

        for (E element : elements) {
            if (set.add(element)) {
                result.add(element);
            }
        }

        return result;
    }

    public static <E> void forEach(List<E> elements, Consumer<E> consumer) {
        for (E element : elements) {
            consumer.accept(element);
        }
    }

    public static <E> Optional<E> reduce(List<E> elements, BinaryOperator<E> accumulator) {
        Iterator<E> iterator = elements.iterator();

        if(!iterator.hasNext()) {
            return Optional.empty();
        }

        return Optional.of(reduce(iterator.next(), elements.subList(1, elements.size()), accumulator));
    }

    public static <E> E reduce(E seed, List<E> elements, BinaryOperator<E> accumulator) {
        E result = seed;

        for (E element : elements) {
            result = accumulator.apply(result, element);
        }

        return result;
    }

    public static <E> Map<Boolean, List<E>> partitionBy(List<E> elements,
                                                        Predicate<E> predicate,
                                                        Supplier<Map<Boolean, List<E>>> mapFactory,
                                                        Supplier<List<E>> listFactory) {
        return partitionByAndMapElement(elements, predicate, mapFactory, listFactory, Function.identity());
    }

    public static <E, T> Map<Boolean, List<T>> partitionByAndMapElement(List<E> elements,
                                                                        Predicate<E> predicate,
                                                                        Supplier<Map<Boolean, List<T>>> mapFactory,
                                                                        Supplier<List<T>> listFactory,
                                                                        Function<E, T> elementMapper) {
        Map<Boolean, List<T>> result = mapFactory.get();
        result.put(true, listFactory.get());
        result.put(false, listFactory.get());

        for (E element : elements) {
            Boolean predicateResult = predicate.test(element);
            List<T> list = result.get(predicateResult);
            T mapperResult = elementMapper.apply(element);

            list.add(mapperResult);
        }

        return result;
    }

    public static <T, K> Map<K, List<T>> groupBy(List<T> elements,
                                                 Function<T, K> classifier,
                                                 Supplier<Map<K, List<T>>> mapFactory,
                                                 Supplier<List<T>> listFactory) {
        return groupByAndMapElement(elements, classifier, mapFactory, listFactory, Function.identity());
    }

    public static <T, U, K> Map<K, List<U>> groupByAndMapElement(List<T> elements,
                                                                 Function<T, K> classifier,
                                                                 Supplier<Map<K, List<U>>> mapFactory,
                                                                 Supplier<List<U>> listFactory,
                                                                 Function<T, U> elementMapper) {
        Map<K, List<U>> result = mapFactory.get();

        for (T element : elements) {
            K classifierResult = classifier.apply(element);
            List<U> list = result.get(classifierResult);
            U mapperResult = elementMapper.apply(element);

            if (list == null) {
                list = listFactory.get();
                result.put(classifierResult, list);
            }
            list.add(mapperResult);
        }

        return result;
    }

    public static <T, K, U> Map<K, U> toMap(List<T> elements,
                                            Function<T, K> keyFunction,
                                            Function<T, U> valueFunction,
                                            BinaryOperator<U> mergeFunction,
                                            Supplier<Map<K, U>> mapFactory) {
        Map<K, U> result = mapFactory.get();

        for (T element : elements) {
            K key = keyFunction.apply(element);
            U value = valueFunction.apply(element);

            if (result.get(key) == null) {
                result.put(key, value);
            } else {
                result.put(key, mergeFunction.apply(result.get(key), value));
            }
        }

        return result;
    }
}