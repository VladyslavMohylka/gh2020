package com.geekhub.hw6.task2;

import java.time.LocalDate;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.IntSummaryStatistics;

public class TaskManager {
    List<Task> find5NearestImportantTasks(List<Task> tasks) {
        return tasks.stream()
            .filter((task) -> task.getType().equals(TaskType.IMPORTANT) &&
                (task.getStartsOn().toEpochDay() - LocalDate.now().toEpochDay()) >= 0 &&
                !task.isDone())
            .sorted((t1, t2) -> (int) (t1.getStartsOn().toEpochDay() - t2.getStartsOn().toEpochDay()))
            .collect(Collectors.toList());
    }

    List<String> getUniqueCategories(List<Task> tasks) {
        return tasks.stream()
            .flatMap((f) -> f.getCategories().stream())
            .distinct()
            .collect(Collectors.toList());
    }

    Map<String, List<Task>> getCategoriesWithTasks(List<Task> tasks) {
        return tasks.stream()
            .flatMap(task -> task.getCategories().stream()
                .map(category -> new AbstractMap.SimpleEntry<>(category, task)))
            .collect(Collectors.groupingBy(
                AbstractMap.SimpleEntry::getKey,
                Collectors.mapping(AbstractMap.SimpleEntry::getValue, Collectors.toList())));
    }

    Map<Boolean, List<Task>> splitTasksIntoDoneAndInProgress(List<Task> tasks) {
        return tasks.stream()
            .collect(Collectors.partitioningBy(Task::isDone));
    }

    boolean existsTaskOfCategory(List<Task> tasks, String category) {
        return tasks.stream()
            .anyMatch(t -> t.getCategories().contains(category));
    }

    String getTitlesOfTasks(List<Task> tasks, int startNo, int endNo) {
        return tasks.stream()
            .skip(startNo)
            .limit(endNo - startNo)
            .map(Task::getTitle)
            .collect(Collectors.joining(", "));
    }

    Map<String, Long> getCountsByCategories(List<Task> tasks) {
        return tasks.stream()
            .map(Task::getCategories)
            .flatMap(Collection::stream)
            .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }

    IntSummaryStatistics getCategoriesNamesLengthStatistics(List<Task> tasks) {
        return tasks.stream()
            .flatMap((f) -> f.getCategories().stream())
            .mapToInt(String::length)
            .summaryStatistics();
    }

    Task findTaskWithBiggestCountOfCategories(List<Task> tasks) {
        return tasks.stream()
            .max(Comparator.comparing((e) -> e.getCategories().size())).orElse(null);
    }
}
