package com.geekhub.hw6.task2;

import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

public class Task {
    private final int id;
    private final TaskType type;
    private final String title;
    private final boolean done;
    private final Set<String> categories;
    private final LocalDate startsOn;

    public Task(int id, TaskType type, String title, boolean done, Set<String> categories, LocalDate startsOn) {
        this.id = id;
        this.type = type;
        this.title = title;
        this.done = done;
        this.categories = categories;
        this.startsOn = startsOn;
    }

    public int getId() {
        return id;
    }

    public TaskType getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public boolean isDone() {
        return done;
    }

    public Set<String> getCategories() {
        return categories;
    }

    public LocalDate getStartsOn() {
        return startsOn;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", type=" + type +
                ", title='" + title + '\'' +
                ", done=" + done +
                ", categories=" + categories +
                ", startsOn=" + startsOn +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return id == task.id &&
                done == task.done &&
                type == task.type &&
                Objects.equals(title, task.title) &&
                Objects.equals(categories, task.categories) &&
                Objects.equals(startsOn, task.startsOn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, title, done, categories, startsOn);
    }
}
