package com.geekhub.hw6.task2;

import java.time.LocalDate;
import java.util.*;

public class Application {
    public static void main(String[] args) {
        List<Task> tasks = new ArrayList<>();
        TaskManager taskManager = new TaskManager();
        Set<String> category1 = new HashSet<>(Arrays.asList("work","rest","learn"));
        Set<String> category2 = new HashSet<>(Arrays.asList("rest","learn"));
        Set<String> category3 = new HashSet<>(Collections.singletonList("learn"));
        Set<String> category4 = new HashSet<>(Arrays.asList("work","rest","learn","sleep"));

        tasks.add(new Task(1, TaskType.IMPORTANT,
                "go to work", true,
                category1, LocalDate.of(2020, 12,12)));

        tasks.add(new Task(2, TaskType.IMPORTANT,
                "go swimming", false,
                category2, LocalDate.of(2020, 12,31)));

        tasks.add(new Task(3, TaskType.NOTIMPORTANT,
                "go learning", false,
                category3, LocalDate.of(2020, 12,23)));

        tasks.add(new Task(4, TaskType.IMPORTANT,
                "go sleeping", false,
                category3, LocalDate.of(2020, 11,12)));

        tasks.add(new Task(5, TaskType.NOTIMPORTANT,
                "go jumping", false,
                category4, LocalDate.of(2020, 12,12)));

        tasks.add(new Task(6, TaskType.IMPORTANT,
                "go rest", false,
                category2, LocalDate.of(2022, 12,12)));

        tasks.add(new Task(7, TaskType.NOTIMPORTANT,
                "go dancing", false,
                category3, LocalDate.of(2021, 12,12)));

   //System.out.println(taskManager.find5NearestImportantTasks(tasks));
   //System.out.println(taskManager.getUniqueCategories(tasks));
   //System.out.println(taskManager.splitTasksIntoDoneAndInProgress(tasks));
   //System.out.println(taskManager.existsTaskOfCategory(tasks, "learn"));
   //System.out.println(taskManager.getTitlesOfTasks(tasks, 3, 7));
   //System.out.println(taskManager.getCountsByCategories(tasks));
   //System.out.println(taskManager.getCategoriesNamesLengthStatistics(tasks));
   //System.out.println(taskManager.getCategoriesWithTasks(tasks));
   //System.out.println(taskManager.findTaskWithBiggestCountOfCategories(tasks));
    }
}
