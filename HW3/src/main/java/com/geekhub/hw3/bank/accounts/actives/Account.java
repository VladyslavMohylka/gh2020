package com.geekhub.hw3.bank.accounts.actives;

public class Account extends Active {
    private final String currency;
    private final double amount;

    public Account(String currency, double amount) {
        this.currency = currency;
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }
}
