package com.geekhub.hw3.bank;

public enum ActivesRate {
    USD(25),
    EUR(30),
    GBP(35),
    SILVER(400),
    GOLD(700),
    PLATINUM(1000);

    private final int value;

    ActivesRate(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
