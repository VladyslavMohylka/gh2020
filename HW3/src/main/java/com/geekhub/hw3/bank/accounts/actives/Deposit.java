package com.geekhub.hw3.bank.accounts.actives;

import java.util.Date;

public class Deposit extends Active {
    private final String currency;
    private final Date startDate;
    private final double moneyAmount;
    private final double yearPercent;
    private final double amountOfYears;

    public Deposit(String currency, double moneyAmount, double yearPercent, double amountOfYears) {
        this.currency = currency;
        this.moneyAmount = moneyAmount;
        this.yearPercent = yearPercent;
        this.amountOfYears = amountOfYears;
        this.startDate = new Date();
    }

    public double getPriceOfDeposit() {
        return moneyAmount + moneyAmount * yearPercent * getTimePassed();
    }

    public String getCurrency() {
        return currency;
    }

    public double getTimePassed() {
       final double millisecondPerYear = 31536000000.0;

       return (new Date().getTime() - startDate.getTime()) / millisecondPerYear;
    }

    public double getMoneyAmount() {
        return moneyAmount;
    }

    public double getYearPercent() {
        return yearPercent;
    }

    public double getAmountOfYears() {
        return amountOfYears;
    }
}
