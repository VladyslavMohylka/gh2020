package com.geekhub.hw3.bank.accounts.actives;

public class Metal extends Active {
    private final String typeOfMetal;
    private final double massInKg;

    public Metal(String typeOfMetal, double massInKg) {
        this.typeOfMetal = typeOfMetal;
        this.massInKg = massInKg;
    }

    public String getTypeOfMetal() {
        return typeOfMetal;
    }

    public double getMassInKg() {
        return massInKg;
    }
}
