package com.geekhub.hw3.bank;

import com.geekhub.hw3.bank.accounts.actives.*;
import com.geekhub.hw3.bank.accounts.clients.*;

public class Application {
    public static void main(String[] args) {
        Bank bank = new Bank();

        bank.newAccount(new Individual("John", "Watson", "individual"));
        bank.newAccount(new Legal("Sherlock", "Holmes", "legal"));
        bank.newAccount(new Legal("Tim", "Roth", "legal"));
        bank.newAccount(new Individual("Zak", "Babak", "individual"));

        bank.addActive(1000, new Metal("gold", 10));
        bank.addActive(1000, new Metal("platinum", 1));
        bank.addActive(1001, new Metal("gold", 8));
        bank.addActive(1001, new Metal("silver", 4));
        bank.addActive(1002, new Metal("gold", 3));
        bank.addActive(1003, new Metal("silver", 1));

        bank.addActive(1000, new Account("dollar", 1230));
        bank.addActive(1001, new Account("dollar", 230));
        bank.addActive(1002, new Account("dollar", 130));
        bank.addActive(1002, new Account("euro", 777));
        bank.addActive(1003, new Account("dollar", 1280));
        bank.addActive(1003, new Account("pound", 1412));

        bank.addActive(1000, new Deposit("pound", 120, 12, 12));
        bank.addActive(1001, new Deposit("dollar", 110, 11, 11));
        bank.addActive(1001, new Deposit("euro", 1200, 14, 3));
        bank.addActive(1002, new Deposit("pound", 999, 17, 1));
        bank.addActive(1002, new Deposit("dollar", 123, 7, 2));
        bank.addActive(1003, new Deposit("dollar", 5432, 12, 7));

        System.out.println(bank.getSumAllActives());
        System.out.println(bank.getAllClients());
        System.out.println(bank.getActives(1001, "metal", "account", "deposit"));
    }
}
