package com.geekhub.hw3.bank.accounts.clients;


public class Client {
    private final String entity;
    private final String name;
    private final String surname;

    public Client(String name, String surname, String entity) {
        this.entity = entity;
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
         return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getEntity() {
         return entity;
    }
}
