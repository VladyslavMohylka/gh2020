package com.geekhub.hw3.bank.accounts;

import com.geekhub.hw3.bank.accounts.actives.*;
import com.geekhub.hw3.bank.accounts.clients.Client;
import com.geekhub.hw3.bank.accounts.actives.Active;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BankAccount {
    private final String accountType;
    private final Client client;
    private final Map<String, ArrayList<Active>> actives ;

    public BankAccount(Client client) {
        this.client = client;
        this.accountType = client.getEntity();
        this.actives = new HashMap<String, ArrayList<Active>>() {{
            put("Account", new ArrayList<>());
            put("Metal", new ArrayList<>());
            put("Deposit", new ArrayList<>());
        }};
    }

    public void addActive(Active active) {
        if (active instanceof Metal) {
            actives.get("Metal").add(active);
        } else if (active instanceof Account) {
            actives.get("Account").add(active);
        } else {
            actives.get("Deposit").add(active);
        }
    }

    public Map<String, ArrayList<Active>> getAllActives() {
       return actives;
    }

    public ArrayList<Active> getAllMetals() {
        return actives.get("Metal");
    }

    public ArrayList<Active> getAllAccounts() {
        return actives.get("Account");
    }

    public ArrayList<Active> getAllDeposits() {
        return actives.get("Deposit");
    }

    public Client getClient() {
        return client;
    }

    public String getAccountType() {
        return accountType;
    }
}
