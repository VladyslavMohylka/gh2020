package com.geekhub.hw3.bank;

import com.geekhub.hw3.bank.accounts.*;
import com.geekhub.hw3.bank.accounts.actives.*;
import com.geekhub.hw3.bank.accounts.clients.*;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class Bank {
    private final Map<Integer, BankAccount> bankAccounts;

    public Bank() {
        this.bankAccounts = new TreeMap<>();
    }

    public void newAccount(Client client) {
        int id = getId(bankAccounts);

        bankAccounts.put(id, new BankAccount(client));
    }

    public void addActive(int id, Active active) {
        BankAccount currentAccount = getBankAccount(id);

        currentAccount.addActive(active);
    }

    public ArrayList<Active> getActives(int id, String ... requiredActives) {
        BankAccount currentAccount = getBankAccount(id);
        ArrayList<Active> respondActives = new ArrayList<>();

        for (String requiredActive : requiredActives) {
            switch (requiredActive) {
                case "metal" :
                    respondActives.addAll(currentAccount.getAllMetals());
                    break;
                case "account" :
                    respondActives.addAll(currentAccount.getAllAccounts());
                    break;
                case "deposit" :
                    respondActives.addAll(currentAccount.getAllDeposits());
                    break;
            }
        }

        return respondActives;
    }

    public ArrayList<Legal> getAllLegalClients() {
        ArrayList<Legal> clients = new ArrayList<>();

        for (int id : bankAccounts.keySet()) {
            BankAccount currentAccount = getBankAccount(id);

            if (currentAccount.getAccountType().equals("legal")) {
                clients.add((Legal)currentAccount.getClient());
            }
        }

        return clients;
    }

    public ArrayList<Individual> getAllIndividualClients() {
        ArrayList<Individual> clients = new ArrayList<>();

        for (int id : bankAccounts.keySet()) {
            BankAccount currentAccount = getBankAccount(id);

            if (currentAccount.getAccountType().equals("individual")) {
                clients.add((Individual)currentAccount.getClient());
            }
        }

        return clients;
    }

    public ArrayList<Client> getAllClients() {
        ArrayList<Client> allClients = new ArrayList<>();

        allClients.addAll(getAllIndividualClients());
        allClients.addAll(getAllLegalClients());

        return allClients;
    }

    public double getSumAllActives() {
        double sum = 0;

        for (Integer key : bankAccounts.keySet()) {
           ArrayList<Active> allAccountActives = getActives(key, "deposit", "metal", "account");

           for (Active currentActive : allAccountActives) {

               if (currentActive instanceof Metal) {
                   Metal metal = (Metal)currentActive;
                   sum += convert(metal.getTypeOfMetal(), metal.getMassInKg());

               } else if(currentActive instanceof Account) {
                   Account account = (Account)currentActive;
                   sum += convert(account.getCurrency(), account.getAmount());

               } else {
                   Deposit deposit = (Deposit)currentActive;
                   sum += convert(deposit.getCurrency(), deposit.getPriceOfDeposit());
               }
           }
        }

        return sum;
    }

    private int getId(Map<Integer, BankAccount> client) {
        int id = 1000;

        while (client.containsKey(id)) {
            id++;
        }

        return id;
    }

    private BankAccount getBankAccount(int id) {
        BankAccount bankAccount = null;

        for (Map.Entry<Integer, BankAccount> currentAccount : bankAccounts.entrySet()) {
            if (currentAccount.getKey() == id) {
                bankAccount = currentAccount.getValue();
            }
        }

        return bankAccount;
    }

    private double convert(String type, double amount) {
        double cost = 0;

        switch (type) {
            case "gold" :
                cost = amount * ActivesRate.GOLD.getValue();
                break;
            case "silver" :
                cost = amount * ActivesRate.SILVER.getValue();
                break;
            case "platinum" :
                cost = amount * ActivesRate.PLATINUM.getValue();
                break;
            case "dollar" :
                cost = amount * ActivesRate.USD.getValue();
                break;
            case "euro" :
                cost = amount * ActivesRate.EUR.getValue();
                break;
            case "pound" :
                cost = amount * ActivesRate.GBP.getValue();
                break;
        }

        return cost;
    }
}
