package com.geekhub.hw3.bank.accounts.clients;

public class Legal extends Client {
    private String internetDirection = null;

    public Legal(String name, String surname, String entity) {
        super(name, surname, entity);
    }

    public String getInternetDirection() {
        return internetDirection;
    }

    public void setInternetDirection(String internetDirection) {
        this.internetDirection = internetDirection;
    }
}
