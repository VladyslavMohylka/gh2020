package com.geekhub.hw3.words;

import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Redaction redact = new Redaction();
        Validation validate = new Validation();
        int arrayLength;
        String[] words;

        System.out.print("enter number words: ");
        arrayLength = scanner.nextInt();
        words = new String[arrayLength];

        for (int i = 0; i < arrayLength; i++) {
            System.out.print("enter word: ");
            words[i] = scanner.next();
        }

        for (int i = 0; i < arrayLength; i++) {
            String word = words[i];
            if (validate.isDigit(word)) {
                continue;
            } else if (validate.length(word)) {
              word = redact.constrict(word);
            }
            System.out.println(word);
        }
    }
}

