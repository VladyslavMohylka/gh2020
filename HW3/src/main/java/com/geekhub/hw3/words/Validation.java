package com.geekhub.hw3.words;

public class Validation {
    public boolean isDigit(String word) {
        return word.chars().allMatch(Character::isDigit);
    }

    public boolean length(String word) {
        return word.length() >= 10;
    }
}
