package com.geekhub.hw8;

import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.List;

public class Filter implements FileFilter {
    private final List<String> allowedTypes = Arrays.asList(".mp3", "jpeg", ".wav", ".wma", ".avi", ".mp4", ".flv", ".jpg", ".gif", ".png");

    public boolean accept(File file) {
        if (file.isDirectory()) {
            return true;
        }
        String name = file.getName();

        return allowedTypes.contains(name.substring(name.length() - 4));
    }
}
