package com.geekhub.hw8;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipElement {
    private final List<File> filteredList = new ArrayList<>();
    private final Filter filter = new Filter();
    private String path;

    public void getZip(String path) throws NullPointerException {
        this.path = Objects.requireNonNull(path);

        addToZip(getFilteredElements(new File(path)));
    }

    private List<File> getFilteredElements(File file) {
        if (file.isFile()) {
            filteredList.add(file);
        } else {
            File[] subFiles = file.listFiles(filter);

            if (!Objects.isNull(subFiles) && subFiles.length != 0) {
                for (File subFile : subFiles) {
                    getFilteredElements(subFile);
                }
            }
        }

        return filteredList;
    }

    private void addToZip(List<File> files) {
        try {
            FileOutputStream fos = new FileOutputStream(path + ".zip");
            ZipOutputStream zos = new ZipOutputStream(fos);

            for (File file : files) {
                FileInputStream fis = new FileInputStream(file);
                ZipEntry ze = new ZipEntry(getFileName(file));

                zos.putNextEntry(ze);
                int buffer = 1024;
                byte[] data = new byte[buffer];
                int count;
                while((count = fis.read(data, 0, buffer)) != -1) {
                    zos.write(data, 0, count);
                }
                zos.closeEntry();
            }
            zos.close();
            fos.close();
        } catch (IOException ioExp) {
            System.err.println("Error while zipping: " + ioExp.getClass().getSimpleName());
            System.err.println(ioExp.getStackTrace()[0]);
        }
    }

    private String getFileName(File file) {
        return file.getPath().substring(path.length() + 1);
    }
}
