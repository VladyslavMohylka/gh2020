package com.geekhub.hw4.task1.sorters;

import com.geekhub.hw4.task1.ArraySorter;
import com.geekhub.hw4.task1.SortDirection;

public class InsertionSortArraySorter implements ArraySorter {
    public void sort(int[] array, SortDirection direction) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i; j >= 0; j--) {
                int a = direction.equals(SortDirection.DESC) ? array[j] : array[j + 1];
                int b = direction.equals(SortDirection.DESC) ? array[j + 1] : array[j];

                if (a < b) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                } else {
                    break;
                }
            }
        }
    }
}
