package com.geekhub.hw4.task1.sorters;

import com.geekhub.hw4.task1.ArraySorter;
import com.geekhub.hw4.task1.SortDirection;

public class SelectionSortArraySorter implements ArraySorter {
    public void sort(int[] array, SortDirection direction) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i; j < array.length - 1; j++) {
                int a = direction.equals(SortDirection.DESC) ? array[i] : array[j + 1];
                int b = direction.equals(SortDirection.DESC) ? array[j + 1] : array[i];

                if (a < b) {
                    int temp = array[i];
                    array[i] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }
}
