package com.geekhub.hw4.task1.sorters;

import com.geekhub.hw4.task1.ArraySorter;
import com.geekhub.hw4.task1.SortDirection;

public class BubbleSortArraySorter implements ArraySorter {
    public void sort(int[] array, SortDirection direction) {
        boolean sorted = false;

        while (!sorted) {
            sorted = true;
            for (int j = 0; j < array.length - 1; j++) {
                int a = direction.equals(SortDirection.DESC) ? array[j] : array[j + 1];
                int b = direction.equals(SortDirection.DESC) ? array[j + 1] : array[j];

                if (a < b) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                    sorted = false;
                }
            }
        }
    }
}
