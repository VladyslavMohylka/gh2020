package com.geekhub.hw4.task1;

import com.geekhub.hw4.task1.sorters.*;
import java.util.Arrays;

public class Application {
    public static void main(String[] args) {
        int[] arr = {12, 2143, 332, 454, 5, 650, 400};
        BubbleSortArraySorter bubbleSort = new BubbleSortArraySorter();
        InsertionSortArraySorter insertionSort = new InsertionSortArraySorter();
        SelectionSortArraySorter selectionSort = new SelectionSortArraySorter();

        System.out.println(Arrays.toString(arr));
        insertionSort.sort(arr, SortDirection.ASC);
        System.out.println(Arrays.toString(arr));
    }
}
