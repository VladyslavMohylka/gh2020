package com.geekhub.hw4.task1;

public interface ArraySorter {
    void sort(int[] array, SortDirection direction);
}
