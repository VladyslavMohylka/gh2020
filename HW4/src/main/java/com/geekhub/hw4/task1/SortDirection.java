package com.geekhub.hw4.task1;

public enum SortDirection {
    ASC(1),
    DESC(-1);

    private final int value;

    SortDirection(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
