package com.geekhub.hw4.task2;

import java.time.LocalDateTime;

public class Application {
    public static void main(String[] args) {
        TaskManagerImpl tasksManager = new TaskManagerImpl();

        LocalDateTime time1 = LocalDateTime.of( 2020, 12, 12, 12, 55);
        LocalDateTime time2 = LocalDateTime.of( 2022, 11, 18, 12, 50);
        LocalDateTime time3 = LocalDateTime.of( 2021, 11, 18, 12, 50);
        LocalDateTime time4 = LocalDateTime.of( 2021, 10, 18, 12, 50);
        LocalDateTime time5 = LocalDateTime.of( 2020, 12, 12, 12, 49);

        tasksManager.add(time1, new Task("Bob", "read"));
        tasksManager.add(time2, new Task("Tom", "write"));
        tasksManager.add(time3, new Task("Din", "read"));
        tasksManager.add(time4, new Task("Baz", "write"));
        tasksManager.add(time5, new Task("Pol", "read"));
        tasksManager.add(LocalDateTime.now(), new Task("Richard", "write"));
        tasksManager.add(LocalDateTime.now(), new Task("Potter", "read"));

        //System.out.println(tasksManager.getTasksByCategories("read", "write"));
        //System.out.println(tasksManager.getCategories());
        //System.out.println(tasksManager.getTasksByCategory("read"));
        //System.out.println(tasksManager.getTasksForToday());
    }
}



