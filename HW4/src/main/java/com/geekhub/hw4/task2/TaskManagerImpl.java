package com.geekhub.hw4.task2;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

public class TaskManagerImpl implements TaskManager {
    private final Map<LocalDateTime, Task> taskManager;

    public TaskManagerImpl() {
        taskManager = new TreeMap<>();
    }

    public void add(LocalDateTime date, Task task) {
        taskManager.put(date, task);
    }

    public void remove(LocalDateTime date) {
        taskManager.remove(date);
    }

    public Set<String> getCategories() {
        Set<String> categories = new HashSet<>();

        for (Task task : taskManager.values()) {
            categories.add(task.getCategory());
        }

        return categories;
    }

    public Map<String, List<Task>> getTasksByCategories(String ... categories) {
        Map<String, List<Task>> tasksByCategories = new HashMap<>();

        for (String category : categories) {
            tasksByCategories.put(category, getTasksByCategory(category));
        }

        return tasksByCategories;
    }

    public List<Task> getTasksByCategory(String category) {
        List<Task> tasksByCategory = new ArrayList<>();

        for (Task task : taskManager.values()) {
            if (task.getCategory().equals(category)) {
                tasksByCategory.add(task);
            }
        }

        return tasksByCategory;
    }

    public List<Task> getTasksForToday() {
        List<Task> tasksForToday = new ArrayList<>();
        LocalDate currentDate = LocalDate.now();

        for (Map.Entry<LocalDateTime, Task> entry : taskManager.entrySet()) {
            LocalDateTime taskDate = entry.getKey();
            Task task = entry.getValue();

            if (taskDate.toLocalDate().equals(currentDate)) {
                tasksForToday.add(task);
            }
        }

        return tasksForToday;
    }
}
